import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SideNavInfoCompanyComponent } from './components/side-nav-info-company/side-nav-info-company.component';
import { FAQComponent } from './components/faq/faq.component';
import { RelatedCategoriesComponent } from './components/related-categories/related-categories.component';
import { SimilarStoresComponent } from './components/similar-stores/similar-stores.component';
import { SideNavRecommendationsComponent } from './components/side-nav-recommendations/side-nav-recommendations.component';
import { SideNavStoresComponent } from './components/side-nav-stores/side-nav-stores.component';
import { CardCouponComponent } from './components/card-coupon/card-coupon.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SideNavInfoCompanyComponent,
    FAQComponent,
    RelatedCategoriesComponent,
    SimilarStoresComponent,
    SideNavRecommendationsComponent,
    SideNavStoresComponent,
    CardCouponComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

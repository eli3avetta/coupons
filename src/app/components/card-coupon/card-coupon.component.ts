import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-coupon',
  templateUrl: './card-coupon.component.html',
  styleUrls: ['./card-coupon.component.scss']
})
export class CardCouponComponent {
  @Input() isPopularCoupon: boolean;
  @Input() disabledCoupon: boolean;
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCouponComponent } from './card-coupon.component';

describe('CouponComponent', () => {
  let component: CardCouponComponent;
  let fixture: ComponentFixture<CardCouponComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardCouponComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

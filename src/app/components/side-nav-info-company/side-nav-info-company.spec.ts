import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavInfoCompanyComponent } from './side-nav-info-company.component';

describe('SidePanelComponent', () => {
  let component: SideNavInfoCompanyComponent;
  let fixture: ComponentFixture<SideNavInfoCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideNavInfoCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavInfoCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimilarStoresComponent } from './similar-stores.component';

describe('SimilarStoresComponent', () => {
  let component: SimilarStoresComponent;
  let fixture: ComponentFixture<SimilarStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimilarStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimilarStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavRecommendationsComponent } from './side-nav-recommendations.component';

describe('SideNavRecommendationsComponent', () => {
  let component: SideNavRecommendationsComponent;
  let fixture: ComponentFixture<SideNavRecommendationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideNavRecommendationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavRecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

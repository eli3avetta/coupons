import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavStoresComponent } from './side-nav-stores.component';

describe('SideNavStoresComponent', () => {
  let component: SideNavStoresComponent;
  let fixture: ComponentFixture<SideNavStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideNavStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
